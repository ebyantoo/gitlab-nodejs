const express = require('express');
var requestIp = require('request-ip');

const app = express();
const port = process.env.PORT || 3000;

app.param('name', function (req, res, next, name) {
    const modified = name.toUpperCase();
    req.name = modified;
    next();
});

//http://localhost:3000/?time=now
app.get('/time', function (req, res) {
    const time_id = req.query.time;
    if (time_id == 'now') {
        res.send({
            'time_id': time_id,
        });
        var date_ob = new Date();
        var hours = date_ob.getHours();
        var minutes = date_ob.getMinutes();
        var seconds = date_ob.getSeconds();
        var dateTime = hours + ":" + minutes + ":" + seconds;
        console.log(dateTime);
    } else {
        console.log("error");
    }

});

//http://localhost:3000/?whoami=1
app.get('/ip', function (req, res) {
    const whoami_id = req.query.whoami;
    if (whoami_id == '1') {
        res.send({
            'whoami_id': whoami_id,
        });

        var clientIp = requestIp.getClientIp(req);
        console.log(clientIp);
    } else {
        console.log("error");
    }

});

app.listen(port);
console.log('Server running at http://localhost:' + port);